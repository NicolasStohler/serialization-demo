# Json / XML / csv Serialization/Deserialization Examples using C# #

Some code examples showing how to write and read .NET POCOs using the following Nuget libraries:

- [Bender](https://github.com/mikeobrien/Bender)
- [ServiceStack.Text](https://github.com/ServiceStack/ServiceStack.Text)
 

