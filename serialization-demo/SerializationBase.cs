﻿using System.Collections.Generic;
using System.IO;
using Bender;
using ServiceStack.Text;

namespace serialization_demo
{
	public abstract class SerializationBase
	{
		public List<Person> GetSampleData()
		{
			var people = new List<Person>
			{
				new Person() { Name = "Nicolas", Alter = 41, Quotes = new List<Quote>() { new Quote() { Text = "Yehaa!", UseCount = 2 } } },
				new Person() { Name = "Patrick", Alter = 45 },
				new Person() { Name = "Steven", Alter = 23 },
				new Person() { Name = "Wurstel", Alter = 33,
					Quotes = new List<Quote>()
					{
						new Quote() { Text = "Kabong!", UseCount = 7 },
						new Quote() { Text = "Hans!", UseCount = 3 },
						new Quote() { Text = "Wurst!", UseCount = 5 }
					}
				},
			};
			return people;
		}

		
	}
}