﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bender;
using ServiceStack.Text;

namespace serialization_demo
{
	public class SerializationBender: SerializationBase
	{
		// Bender library (https://github.com/mikeobrien/Bender)

		// JSON
		public void WriteJsonFile(string filename)
		{
			var people = GetSampleData();

			people.SerializeJsonFile(filename);
		}

		public void ReadJsonFile(string filename)
		{
			var people = Deserialize.JsonFile(filename, typeof(List<Person>));

			people.PrintDump();
		}

		// XML
		public void WriteXmlFile(string xmlFilename)
		{
			var people = GetSampleData();

			people.SerializeXmlFile(xmlFilename);
		}

		public void ReadXmlFile(string xmlFilename)
		{
			var people = Deserialize.XmlFile(xmlFilename, typeof(List<Person>));

			people.PrintDump();
		}

	}
}
