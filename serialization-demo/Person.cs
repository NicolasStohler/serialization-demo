﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialization_demo
{
	public class Person
	{
		public string Name { get; set; }
		public int Alter { get; set; }
		public List<Quote> Quotes { get; set; }

		public string GetInfo()
		{
			return $"{Name} is {Alter}";
		}
	}
}
