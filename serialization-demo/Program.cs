﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialization_demo
{
	class Program
	{
		static void Main(string[] args)
		{
			
			string jsonFilename = @"persons.json";
			string jsonFilenameSS = @"personsServiceStack.json";
			string xmlFilename = @"persons.xml";
			string csvFilename = @"persons.csv";

			var serializationBender = new SerializationBender();

			serializationBender.WriteJsonFile(jsonFilename);
			serializationBender.ReadJsonFile(jsonFilename);

			serializationBender.WriteXmlFile(xmlFilename);
			serializationBender.ReadXmlFile(xmlFilename);

			var serializationServiceStackText = new SerializationServiceStack_Text();

			serializationServiceStackText.WriteJsonFile(jsonFilenameSS);
			serializationServiceStackText.ReadJsonFile(jsonFilenameSS);

			serializationServiceStackText.WriteCsvFile(csvFilename);
			serializationServiceStackText.ReadCsvFile(csvFilename);

			Console.WriteLine("done.");
			Console.ReadLine();
		}
	}
}
