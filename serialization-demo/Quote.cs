﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serialization_demo
{
	public class Quote
	{
		public string Text { get; set; }
		public int UseCount { get; set; }
	}
}
