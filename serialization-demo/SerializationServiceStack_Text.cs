﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.Text;

namespace serialization_demo
{
	public class SerializationServiceStack_Text : SerializationBase
	{
		// https://github.com/ServiceStack/ServiceStack.Text

		// JSON
		public void WriteJsonFile(string filename)
		{
			using (var filestream = File.Create(filename))
			{
				var people = GetSampleData();
				JsonSerializer.SerializeToStream(people, filestream);
			}
		}

		public void ReadJsonFile(string filename)
		{
			using (var filestream = File.Open(filename, FileMode.Open, FileAccess.Read))
			{
				var people = JsonSerializer.DeserializeFromStream<List<Person>>(filestream);
				people.PrintDump();
			}
		}

		// XML

		// CSV
		public void WriteCsvFile(string filename)
		{
			using (var filestream = File.Create(filename))
			{
				var people = GetSampleData();
				CsvSerializer.SerializeToStream(people, filestream);
			}
		}

		public void ReadCsvFile(string filename)
		{
			using (var filestream = File.Open(filename, FileMode.Open, FileAccess.Read))
			{
				var people = CsvSerializer.DeserializeFromStream<List<Person>>(filestream);
				people.PrintDump();
			}
		}
	}
}
